FROM docker.gitlab.gwdg.de/mythen/mythen-server:main

COPY . /var/www/html/

RUN php composer.phar install --prefer-dist --no-progress --no-suggest --optimize-autoloader --classmap-authoritative  --no-interaction && \
    php composer.phar clear-cache && \
    mkdir -p /var/www/html/var/cache/ && \
    chmod -R 777 /var/www/html/var

VOLUME /var/www/html/
