# Mythen

This is the base package as web application for the mythology project of the Göttingen University.

The Backend is using Symfony as PHP Framework.
The API can be found at `/api`.

## Requirements

You need PHP 8.04 installed. For storing data, a 
[supported](https://www.doctrine-project.org/projects/doctrine-dbal/en/2.8/reference/platforms.html)
database engine is required. 
Also see [http://symfony.com/doc/current/reference/requirements.html](http://symfony.com/doc/current/reference/requirements.html).

## Installation

* Clone this repository
* `git config --add core.hooksPath .githooks`
* `composer install`
* `./bin/console doctrine:database:create`
* `./bin/console doctrine:schema:drop --force && ./bin/console doctrine:schema:create && y | ./bin/console doctrine:fixtures:load`
* `symfony server:start`

The admin user "admin" has the password "admin". It is created like the other fixtures after running `./bin/console doctrine:fixtures:load`.

### JWT authentication

The ReactAdmin needs to authenticate with username and password to query the API. Therefore we use JSON Web Tokens (JWT, [https://jwt.io](https://jwt.io)).
For local development you have to generate a JWT SSH keypair and provide the private and public keys as PEM files `private.pem` and `public.pem` in `/config/jwt/`.
Generating those keys can be done like this way:
```shell
ssh-keygen -t rsa -b 4096 -m PEM -f jwtRS256.key
# Don't add passphrase
openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwtRS256.key.pub
cat jwtRS256.key
cat jwtRS256.key.pub
```

### Running with docker

* `docker-compose up -d`
* `git config --add core.hooksPath .githooks`
* `docker-compose exec web composer install`
* optional `docker-compose exec web ./bin/console doctrine:database:create`
* `docker-compose exec web ./bin/console doctrine:schema:drop --force && docker-compose exec web ./bin/console doctrine:schema:create && docker-compose exec web ./bin/console doctrine:fixtures:load -n`
* Continue with the frontend assets as described in the `Frontend` section above
* Open the browser and point it to [localhost:8866](http://localhost:8866)

### Solr Data

After the docker-compose steps, run 

* `docker-compose exec solr precreate-core myth`
* `curl http://localhost:8913/solr/admin/cores?action=RELOAD&core=myth`
* `docker-compose exec solr bin/post -c myth /usr/src/app/docs.xml`
