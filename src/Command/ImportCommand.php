<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Hylem;
use App\Entity\HylemSequence;
use App\Entity\HylemSequenceItem;
use App\Import\ImportUtility;
use App\Import\SolrImport;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends Command
{
    protected static $defaultName = 'data:import';
    private OutputInterface $output;

    public function __construct(
        private SolrImport $solrImport,
        private EntityManagerInterface $manager,
        private ImportUtility $importUtility,
        private LoggerInterface $logger)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Imports data from Solr');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->load();

        return 0;
    }

    /**
     * @psalm-suppress UndefinedClass
     */
    private function isHylemExisting(string $lineInfo, string $hspo): bool
    {
        $hylem = $this->manager
            ->getRepository(Hylem::class)
            ->findOneBy(['textReference' => $lineInfo, 'hySpo' => $hspo]);

        return null !== $hylem;
    }

    /**
     * @psalm-suppress UndefinedClass
     */
    private function isHylemSequenceExisting(string $id): bool
    {
        $hylemSequence = $this->manager
            ->getRepository(HylemSequence::class)
            ->findOneBy([
                'name' => $id,
            ]);

        return null !== $hylemSequence;
    }

    private function load(): void
    {
        ini_set('memory_limit', '-1');

        $data = $this->solrImport->getResults();
        $this->output->writeln(sprintf('Importing %d Hylemes.', count($data)));

        $progressBar = new ProgressBar($this->output, count($data));
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% -- %message%');
        $progressBar->setFormat('custom');

        foreach ($data as $hylemData) {
            $lineInfo = $hylemData['line_info'];
            if ($lineInfo && is_array($lineInfo)) {
                $lineInfo = implode(', ', $hylemData['line_info']);
            }

            $hspo = $hylemData['hspo'];
            if ($hspo && is_array($hspo)) {
                $hspo = implode(', ', $hylemData['hspo']);
            }

            if ($this->isHylemSequenceExisting($hylemData['source'])) {
                $this->logger->info(sprintf('Hylemsequence %s already exists', $hylemData['source']));

                // hylem sequence _AND_ hylem are already existing?
                if ($this->isHylemExisting($lineInfo, $hspo)) {
                    $this->logger->info(sprintf('Hylem with line info %s and HSPO %s already exists', $lineInfo, $hspo));

                    continue;
                }
            }

            $hylem = new Hylem();

            $hylem
                ->setTextReference($lineInfo)
                ->setHySpo($hspo)
                ->setImplicit(false)
                // There is no Solr field 'indirect'. Therefore we setIndirect to the inverted value of 'speach'.
                ->setIndirect(is_bool($hylemData['speach']) ? !$hylemData['speach'] : true)
                ->setSubjectDeterminations($this->importUtility->getSubjectDeterminations($hylemData, $hylem))
                ->setPredicateDetermination($this->importUtility->getPredicateDetermination($hylemData, $hylem))
                ->setHylemObjectDeterminations($this->importUtility->getObjectDeterminations($hylemData, $hylem));

            $hylemSequenceItem = new HylemSequenceItem();

            $hylemSequence = $this->importUtility->getHylemSequence($hylemData);

            $hylem->addHylemSequenceItem($hylemSequenceItem);
            $hylemSequenceItem
                ->setHylem($hylem)
                ->setItemType('Hylem')
                ->setStorySort($hylemSequence->getItemsTime()->count())
                ->setChronSort($hylemSequence->getItemsTime()->count());

            $hylemSequence->addItemsTime($hylemSequenceItem);

            $progressBar->setMessage(sprintf('Sequenz: %s, Hylem: %s', $hylemSequence->getName(), $hspo));

            $this->manager->persist($hylemSequence);
            $this->logger->info('Sequence was written', [$hylemSequence]);
            $this->manager->flush();

            $progressBar->advance();
        }
        $this->manager->flush();
        $this->output->writeln([PHP_EOL, sprintf('Done in %d seconds.', time() - $progressBar->getStartTime())]);
        $progressBar->finish();
    }
}
