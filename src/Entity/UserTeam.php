<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserTeamRepository")
 */
class UserTeam implements \Stringable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $teamDescription = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private string $teamName = '';

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="team", cascade={"persist"})
     */
    private Collection $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->teamName;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addTeam($this);
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeamDescription(): ?string
    {
        return $this->teamDescription;
    }

    public function getTeamName(): ?string
    {
        return $this->teamName;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeTeam($this);
        }

        return $this;
    }

    public function setTeamDescription(?string $teamDescription): self
    {
        $this->teamDescription = $teamDescription;

        return $this;
    }

    public function setTeamName(string $teamName): self
    {
        $this->teamName = $teamName;

        return $this;
    }

    public function setUsers(Collection $users): self
    {
        $this->users = $users;

        return $this;
    }
}
