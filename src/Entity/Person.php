<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']])]
class Person implements \Stringable
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $gndID = null;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @ApiProperty(iri="http://schema.org/name")
     */
    private string $name = '';

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Source", mappedBy="person", cascade={"persist"})
     */
    private Collection $sources;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $wikidataID = null;

    public function __construct()
    {
        $this->sources = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function addSource(Source $source): self
    {
        if (!$this->sources->contains($source)) {
            $this->sources[] = $source;
            $source->addPerson($this);
        }

        return $this;
    }

    public function getGndID(): ?string
    {
        return $this->gndID;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getSources(): Collection
    {
        return $this->sources;
    }

    public function getWikidataID(): ?string
    {
        return $this->wikidataID;
    }

    public function removeSource(Source $source): self
    {
        if ($this->sources->contains($source)) {
            $this->sources->removeElement($source);
            $source->removePerson($this);
        }

        return $this;
    }

    public function setGndID(?string $gndID): self
    {
        $this->gndID = $gndID;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setSources(Collection $sources): self
    {
        $this->sources = $sources;

        return $this;
    }

    public function setWikidataID(?string $wikidataID): self
    {
        $this->wikidataID = $wikidataID;

        return $this;
    }
}
