<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SourceObjectRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']])]
#[ApiFilter(OrderFilter::class, properties: ['id', 'objectType'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(SearchFilter::class, properties: ['objectType' => 'partial'])]
class SourceObject implements \Stringable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @ApiProperty(iri="http://schema.org/name")
     */
    private string $objectType = '';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Source", mappedBy="objectType", orphanRemoval=true, cascade={"persist"})
     */
    private Collection $sources;

    public function __construct()
    {
        $this->sources = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->objectType;
    }

    public function addSource(Source $source): self
    {
        if (!$this->sources->contains($source)) {
            $this->sources[] = $source;
            $source->setObjectType($this);
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getObjectType(): ?string
    {
        return $this->objectType;
    }

    public function getSources(): Collection
    {
        return $this->sources;
    }

    public function removeSource(Source $source): self
    {
        if ($this->sources->contains($source)) {
            $this->sources->removeElement($source);
            // set the owning side to null (unless already changed)
            if ($source->getObjectType() === $this) {
                $source->setObjectType(null);
            }
        }

        return $this;
    }

    public function setObjectType(string $objectType): self
    {
        $this->objectType = $objectType;

        return $this;
    }

    public function setSources(Collection $sources): self
    {
        $this->sources = $sources;

        return $this;
    }
}
