<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HylemRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']])]
#[ApiFilter(OrderFilter::class, properties: ['id', 'hySpo', 'textReference'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(SearchFilter::class, properties: ['hySpo' => 'partial', 'textReference' => 'partial', 'originalText' => 'partial'])]
class Hylem implements \Stringable
{
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HylemObjectDetermination", mappedBy="hylem", cascade={"persist"})
     */
    private Collection $hylemObjectDeterminations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HylemSequenceItem", mappedBy="hylem", cascade={"persist"})
     */
    private Collection $hylemSequenceItems;

    /**
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/name")
     */
    private ?string $hySpo = null;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $implicit = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $indirect = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $notes = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $originalText = '';

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PredicateDetermination", inversedBy="hylem", orphanRemoval=true, cascade={"persist"})
     */
    private ?PredicateDetermination $predicateDetermination = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SubjectDetermination", mappedBy="hylem", cascade={"persist"})
     */
    private Collection $subjectDeterminations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $textReference = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $translation = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Type", inversedBy="hylems", cascade={"persist"})
     */
    private ?Type $type;

    public function __construct()
    {
        $this->subjectDeterminations = new ArrayCollection();
        $this->hylemObjectDeterminations = new ArrayCollection();
        $this->hylemSequenceItems = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->textReference.' - '.$this->hySpo;
    }

    public function addHylemObjectDetermination(HylemObjectDetermination $hylemObjectDetermination): self
    {
        if (!$this->hylemObjectDeterminations->contains($hylemObjectDetermination)) {
            $this->hylemObjectDeterminations->add($hylemObjectDetermination);
            $hylemObjectDetermination->setHylem($this);
        }

        return $this;
    }

    public function addHylemSequenceItem(HylemSequenceItem $hylemSequenceItem): self
    {
        if (!$this->hylemSequenceItems->contains($hylemSequenceItem)) {
            $this->hylemSequenceItems->add($hylemSequenceItem);
            $hylemSequenceItem->setHylem($this);
        }

        return $this;
    }

    public function addSubjectDetermination(SubjectDetermination $subjectDetermination): self
    {
        if (!$this->subjectDeterminations->contains($subjectDetermination)) {
            $this->subjectDeterminations->add($subjectDetermination);
            $subjectDetermination->setHylem($this);
        }

        return $this;
    }

    public function getHylemObjectDeterminations(): Collection
    {
        return $this->hylemObjectDeterminations;
    }

    public function getHylemSequenceItems(): Collection
    {
        return $this->hylemSequenceItems;
    }

    public function getHySpo(): ?string
    {
        return $this->hySpo;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function getOriginalText(): ?string
    {
        return $this->originalText;
    }

    public function getPredicateDetermination(): ?PredicateDetermination
    {
        return $this->predicateDetermination;
    }

    public function getSubjectDeterminations(): Collection
    {
        return $this->subjectDeterminations;
    }

    public function getTextReference(): ?string
    {
        return $this->textReference;
    }

    public function getTranslation(): ?string
    {
        return $this->translation;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function isImplicit(): bool
    {
        return $this->implicit;
    }

    public function isIndirect(): bool
    {
        return $this->indirect;
    }

    public function removeHylemObjectDetermination(HylemObjectDetermination $hylemObjectDetermination): self
    {
        if ($this->hylemObjectDeterminations->contains($hylemObjectDetermination)) {
            $this->hylemObjectDeterminations->removeElement($hylemObjectDetermination);
            // set the owning side to null (unless already changed)
            if ($hylemObjectDetermination->getHylem() === $this) {
                $hylemObjectDetermination->setHylem(null);
            }
        }

        return $this;
    }

    public function removeHylemSequenceItem(HylemSequenceItem $hylemSequenceItem): self
    {
        if ($this->hylemSequenceItems->contains($hylemSequenceItem)) {
            $this->hylemSequenceItems->removeElement($hylemSequenceItem);
            // set the owning side to null (unless already changed)
            if ($hylemSequenceItem->getHylem() === $this) {
                $hylemSequenceItem->setHylem(null);
            }
        }

        return $this;
    }

    public function removePredicateDetermination(PredicateDetermination $predicateDetermination): self
    {
        if ($this->predicateDetermination === $predicateDetermination) {
            $this->predicateDetermination->setPredicateDetermination(null);
            // set the owning side to null (unless already changed)
            if ($predicateDetermination->getHylem() === $this) {
                $predicateDetermination->setHylem(null);
            }
        }

        return $this;
    }

    public function removeSubjectDetermination(SubjectDetermination $subjectDetermination): self
    {
        if ($this->subjectDeterminations->contains($subjectDetermination)) {
            $this->subjectDeterminations->removeElement($subjectDetermination);
            // set the owning side to null (unless already changed)
            if ($subjectDetermination->getHylem() === $this) {
                $subjectDetermination->setHylem(null);
            }
        }

        return $this;
    }

    public function setHylemObjectDeterminations(Collection $hylemObjectDeterminations): Hylem
    {
        $this->hylemObjectDeterminations = $hylemObjectDeterminations;

        return $this;
    }

    public function setHylemSequenceItems(Collection $hylemSequenceItems): Hylem
    {
        $this->hylemSequenceItems = $hylemSequenceItems;

        return $this;
    }

    public function setHySpo(?string $hySpo): self
    {
        $this->hySpo = $hySpo;

        return $this;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setImplicit(bool $implicit): self
    {
        $this->implicit = $implicit;

        return $this;
    }

    public function setIndirect(bool $indirect): self
    {
        $this->indirect = $indirect;

        return $this;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function setOriginalText(?string $originalText): self
    {
        $this->originalText = $originalText;

        return $this;
    }

    public function setPredicateDetermination(?PredicateDetermination $predicateDetermination): self
    {
        $this->predicateDetermination = $predicateDetermination;

        return $this;
    }

    public function setSubjectDeterminations(Collection $subjectDeterminations): self
    {
        $this->subjectDeterminations = $subjectDeterminations;

        return $this;
    }

    public function setTextReference(?string $textReference): self
    {
        $this->textReference = $textReference;

        return $this;
    }

    public function setTranslation(?string $translation): self
    {
        $this->translation = $translation;

        return $this;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }
}
