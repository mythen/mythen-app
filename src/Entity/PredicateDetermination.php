<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PredicateDeterminationRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']], normalizationContext: ['groups' => ['predicateDetermination']])]
class PredicateDetermination implements \Stringable
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Hylem", mappedBy="predicateDetermination", cascade={"persist"})
     * @Groups("predicateDetermination")
     */
    private ?Hylem $hylem = null;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Predicate", inversedBy="predicateDeterminations", cascade={"persist"})
     * @Groups("predicateDetermination")
     */
    private ?Predicate $predicate = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("predicateDetermination")
     */
    private ?string $predicateDetermination = null;

    public function __toString(): string
    {
        if (null !== $this->predicate) {
            return (string) $this->predicate->getPredicate();
        }

        return (string) $this->predicate;
    }

    public function addHylem(Hylem $hylem): self
    {
        $this->hylem = $hylem;

        return $this;
    }

    public function getHylem(): ?Hylem
    {
        return $this->hylem;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPredicate(): ?Predicate
    {
        return $this->predicate;
    }

    public function getPredicateDetermination(): ?string
    {
        return $this->predicateDetermination;
    }

    public function setHylem(?Hylem $hylem): self
    {
        $this->hylem = $hylem;

        return $this;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setPredicate(?Predicate $predicate): self
    {
        $this->predicate = $predicate;

        return $this;
    }

    public function setPredicateDetermination(?string $predicateDetermination): self
    {
        $this->predicateDetermination = $predicateDetermination;

        return $this;
    }
}
