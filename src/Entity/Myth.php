<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MythRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']])]
#[ApiFilter(OrderFilter::class, properties: ['id', 'title'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(SearchFilter::class, properties: ['title' => 'partial'])]
class Myth implements \Stringable
{
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\HylemSequence", inversedBy="myths", cascade={"persist"})
     */
    private Collection $hylemSequences;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Source", mappedBy="myths", cascade={"persist"})
     */
    private Collection $sources;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[ApiProperty(iri: 'http://schema.org/name')]
    private string $title = '';

    public function __construct()
    {
        $this->sources = new ArrayCollection();
        $this->hylemSequences = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->title;
    }

    public function addHylemSequence(HylemSequence $hylemSequence): void
    {
        if (!$this->hylemSequences->contains($hylemSequence)) {
            $this->hylemSequences->add($hylemSequence);
            $hylemSequence->addMyth($this);
        }
    }

    public function addSource(Source $source): void
    {
        if (!$this->sources->contains($source)) {
            $this->sources->add($source);
            $source->addMyth($this);
        }
    }

    public function getHylemSequences(): Collection
    {
        return $this->hylemSequences;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSources(): array
    {
        return $this->sources->getValues();
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function removeHylemSequence(HylemSequence $hylemSequence): void
    {
        if ($this->hylemSequences->contains($hylemSequence)) {
            $this->hylemSequences->removeElement($hylemSequence);
            $hylemSequence->removeMyth($this);
        }
    }

    /**
     * @psalm-suppress InvalidArgument
     */
    public function removeSource(Source $source): self
    {
        $this->sources->remove($source);
        $source->removeMyth($this);

        return $this;
    }

    public function setHylemSequences(Collection $hylemSequences): self
    {
        $this->hylemSequences = $hylemSequences;

        return $this;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
