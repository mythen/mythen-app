<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HylemSequenceItemRepository")
 */
#[ApiResource]
class HylemSequenceItem implements \Stringable
{
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $chronSort = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Gap", inversedBy="hylemSequenceItems", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Gap $gap = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Headline", inversedBy="hylemSequenceItems", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Headline $headline = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hylem", inversedBy="hylemSequenceItems", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Hylem $hylem = null;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HylemSequence", inversedBy="itemsTime", cascade={"persist"})
     * @ORM\JoinColumn(name="hylem_sequence_id", referencedColumnName="id")
     */
    private ?HylemSequence $itemsTime = null;

    /**
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/name")
     */
    private ?string $itemType = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $storySort = null;

    public function __toString(): string
    {
        if (null !== $this->hylem) {
            return (string) ('' !== $this->hylem->getOriginalText() ? $this->hylem->getOriginalText() : $this->hylem->getHySpo());
        }

        return (string) $this->id;
    }

    public function getChronSort(): ?int
    {
        return $this->chronSort;
    }

    public function getGap(): ?Gap
    {
        return $this->gap;
    }

    public function getHeadline(): ?Headline
    {
        return $this->headline;
    }

    public function getHylem(): ?Hylem
    {
        return $this->hylem;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItemsTime(): ?HylemSequence
    {
        return $this->itemsTime;
    }

    public function getItemType(): ?string
    {
        return $this->itemType;
    }

    public function getStorySort(): ?int
    {
        return $this->storySort;
    }

    public function setChronSort(?int $chronSort): self
    {
        $this->chronSort = $chronSort;

        return $this;
    }

    public function setGap(?Gap $gap): self
    {
        $this->gap = $gap;

        return $this;
    }

    public function setHeadline(?Headline $headline): self
    {
        $this->headline = $headline;

        return $this;
    }

    public function setHylem(?Hylem $hylem): self
    {
        $this->hylem = $hylem;

        return $this;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setItemsTime(?HylemSequence $itemsTime): self
    {
        $this->itemsTime = $itemsTime;

        return $this;
    }

    public function setItemType(?string $itemType): self
    {
        $this->itemType = $itemType;

        return $this;
    }

    public function setStorySort(?int $storySort): self
    {
        $this->storySort = $storySort;

        return $this;
    }
}
