<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GapRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']])]
class Gap implements \Stringable
{
    /**
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/name")
     */
    private ?string $gapRemarks = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HylemSequenceItem", mappedBy="gap", cascade={"persist"})
     */
    private Collection $hylemSequenceItems;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    public function __construct()
    {
        $this->hylemSequenceItems = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->gapRemarks;
    }

    public function addHylemSequenceItem(HylemSequenceItem $hylemSequenceItem): self
    {
        if (!$this->hylemSequenceItems->contains($hylemSequenceItem)) {
            $this->hylemSequenceItems->add($hylemSequenceItem);
            $hylemSequenceItem->setGap($this);
        }

        return $this;
    }

    public function getGapRemarks(): ?string
    {
        return $this->gapRemarks;
    }

    public function getHylemSequenceItems(): Collection
    {
        return $this->hylemSequenceItems;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function removeHylemSequenceItem(HylemSequenceItem $hylemSequenceItem): self
    {
        if ($this->hylemSequenceItems->contains($hylemSequenceItem)) {
            $this->hylemSequenceItems->removeElement($hylemSequenceItem);
            // set the owning side to null (unless already changed)
            if ($hylemSequenceItem->getGap() === $this) {
                $hylemSequenceItem->setGap(null);
            }
        }

        return $this;
    }

    public function setGapRemarks(?string $gapRemarks): self
    {
        $this->gapRemarks = $gapRemarks;

        return $this;
    }

    public function setHylemSequenceItems(Collection $hylemSequenceItems): Gap
    {
        $this->hylemSequenceItems = $hylemSequenceItems;

        return $this;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }
}
