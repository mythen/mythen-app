<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SourceRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']])]
#[ApiFilter(SearchFilter::class, properties: ['title' => 'partial', 'titleShort' => 'partial', 'genre' => 'partial'])]
#[ApiFilter(OrderFilter::class, properties: [
    'id',
    'title',
    'titleShort',
    'genre',
], arguments: ['orderParameterName' => 'order'])]
class Source implements \Stringable
{
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $contextualization = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $created;

    /**
     * @ORM\Embedded(class="CustomDate")
     */
    private ?CustomDate $date = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $editor = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $genre = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\HylemSequence", inversedBy="sources", cascade={"persist"})
     */
    private Collection $hylemSequences;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $language = '';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SourceLocality", inversedBy="sources", cascade={"persist"})
     */
    private ?SourceLocality $locality = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $localityContext = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $modified = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Myth", inversedBy="sources")
     */
    private Collection $myths;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SourceObject", inversedBy="sources", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private ?SourceObject $objectType = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Person", inversedBy="sources", cascade={"persist"})
     */
    private Collection $person;

    /**
     * @ORM\Column(type="text")
     */
    private string $referenceText = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $referenceUrl = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @ApiProperty(iri="http://schema.org/name")
     */
    private string $title = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $titleShort = null;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->setCreated(new DateTime());
        if (null === $this->getModified()) {
            $this->setModified(new DateTime());
        }
        $this->person = new ArrayCollection();
        $this->hylemSequences = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->title;
    }

    public function addHylemSequence(HylemSequence $hylemSequence): void
    {
        if (!$this->hylemSequences->contains($hylemSequence)) {
            $this->hylemSequences->add($hylemSequence);
            $hylemSequence->addSource($this);
        }
    }

    public function addMyth(Myth $myth): void
    {
        $this->myths->add($myth);
        $myth->addSource($this);
    }

    public function addPerson(Person $person): self
    {
        if (!$this->person->contains($person)) {
            $this->person[] = $person;
            $person->addSource($this);
        }

        return $this;
    }

    public function getContextualization(): ?string
    {
        return $this->contextualization;
    }

    public function getCreated(): ?DateTimeInterface
    {
        return $this->created;
    }

    public function getDate(): ?CustomDate
    {
        return $this->date;
    }

    public function getEditor(): ?string
    {
        return $this->editor;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function getHylemSequences(): ?Collection
    {
        return $this->hylemSequences;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function getLocality(): ?SourceLocality
    {
        return $this->locality;
    }

    public function getLocalityContext(): ?string
    {
        return $this->localityContext;
    }

    public function getModified(): ?DateTimeInterface
    {
        return $this->modified;
    }

    public function getMyths(): array
    {
        return $this->myths->getValues();
    }

    public function getObjectType(): ?SourceObject
    {
        return $this->objectType;
    }

    public function getPerson(): ?Collection
    {
        return $this->person;
    }

    public function getReferenceText(): ?string
    {
        return $this->referenceText;
    }

    public function getReferenceUrl(): ?string
    {
        return $this->referenceUrl;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getTitleShort(): ?string
    {
        return $this->titleShort;
    }

    public function removeHylemSequence(HylemSequence $hylemSequence): void
    {
        if ($this->hylemSequences->contains($hylemSequence)) {
            $this->hylemSequences->removeElement($hylemSequence);
            $hylemSequence->removeSource($this);
        }
    }

    /**
     * @psalm-suppress InvalidArgument
     */
    public function removeMyth(Myth $myth): self
    {
        $this->myths->remove($myth);
        $myth->removeSource($this);

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->person->contains($person)) {
            $this->person->removeElement($person);
            $person->removeSource($this);
        }

        return $this;
    }

    public function setContextualization(?string $contextualization): self
    {
        $this->contextualization = $contextualization;

        return $this;
    }

    public function setCreated(DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function setDate(?CustomDate $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function setEditor(string $editor): self
    {
        $this->editor = $editor;

        return $this;
    }

    public function setGenre(?string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function setHylemSequences(Collection $hylemSequences): self
    {
        $this->hylemSequences = $hylemSequences;

        return $this;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function setLocality(?SourceLocality $locality): self
    {
        $this->locality = $locality;

        return $this;
    }

    public function setLocalityContext(?string $localityContext): self
    {
        $this->localityContext = $localityContext;

        return $this;
    }

    public function setModified(DateTimeInterface $modified): self
    {
        $this->modified = $modified;

        return $this;
    }

    public function setObjectType(?SourceObject $objectType): self
    {
        $this->objectType = $objectType;

        return $this;
    }

    public function setPerson(Collection $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function setReferenceText(string $referenceText): self
    {
        $this->referenceText = $referenceText;

        return $this;
    }

    public function setReferenceUrl(?string $referenceUrl): self
    {
        $this->referenceUrl = $referenceUrl;

        return $this;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function setTitleShort(?string $titleShort): self
    {
        $this->titleShort = $titleShort;

        return $this;
    }
}
