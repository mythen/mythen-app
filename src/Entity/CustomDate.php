<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class CustomDate implements \Stringable
{
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private ?DateTimeInterface $end = null;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private ?DateTimeInterface $start = null;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $textual = null;

    public function __toString(): string
    {
        return (string) $this->textual;
    }

    public function getEnd(): ?DateTimeInterface
    {
        return $this->end;
    }

    public function getStart(): ?DateTimeInterface
    {
        return $this->start;
    }

    public function getTextual(): ?string
    {
        return $this->textual;
    }

    public function setEnd(?DateTime $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function setStart(?DateTime $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function setTextual(?string $textual): self
    {
        $this->textual = $textual;

        return $this;
    }
}
