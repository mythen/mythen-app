<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']])]
#[ApiFilter(OrderFilter::class, properties: ['id', 'type'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(SearchFilter::class, properties: ['type' => 'partial'])]
class Type implements \Stringable
{
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Hylem", mappedBy="type", cascade={"persist"})
     */
    private Collection $hylems;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @ApiProperty(iri="http://schema.org/name")
     */
    private string $type = '';

    public function __construct()
    {
        $this->hylems = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->type;
    }

    public function addHylem(Hylem $hylem): self
    {
        if (!$this->hylems->contains($hylem)) {
            $this->hylems->add($hylem);
            $hylem->setType($this);
        }

        return $this;
    }

    public function getHylems(): Collection
    {
        return $this->hylems;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setHylems(Collection $hylems): self
    {
        $this->hylems = $hylems;

        return $this;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
