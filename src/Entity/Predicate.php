<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PredicateRepository")
 * @ORM\Cache(region="default", usage="NONSTRICT_READ_WRITE")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']])]
#[ApiFilter(OrderFilter::class, properties: ['id', 'predicate'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(SearchFilter::class, properties: ['predicate' => 'partial'])]
class Predicate implements \Stringable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @ApiProperty(iri="http://schema.org/name")
     * @Groups({"predicateDetermination"})
     */
    private string $predicate = '';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PredicateDetermination", mappedBy="predicate", cascade={"persist"})
     */
    private ?Collection $predicateDeterminations = null;

    public function __construct(string $predicate = '')
    {
        $this->predicateDeterminations = new ArrayCollection();
        if ('' !== $predicate) {
            $this->predicate = $predicate;
        }
    }

    public function __toString(): string
    {
        return $this->predicate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPredicate(): ?string
    {
        return $this->predicate;
    }

    public function getPredicateDeterminations(): ?Collection
    {
        return $this->predicateDeterminations;
    }

    public function setPredicate(string $predicate): self
    {
        $this->predicate = $predicate;

        return $this;
    }

    public function setPredicateDeterminations(?Collection $predicateDetermination): self
    {
        $this->predicateDeterminations = $predicateDetermination;

        return $this;
    }
}
