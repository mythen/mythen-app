<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubjectRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']])]
#[ApiFilter(OrderFilter::class, properties: ['id', 'subject'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(SearchFilter::class, properties: ['subject' => 'partial'])]
class Subject implements \Stringable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @ApiProperty(iri="http://schema.org/name")
     * @ApiFilter(SearchFilter::class, strategy="partial")
     * @Groups({"subjectDetermination"})
     */
    private string $subject = '';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SubjectDetermination", mappedBy="subject", cascade={"persist"})
     */
    private Collection $subjectDeterminations;

    public function __construct()
    {
        $this->subjectDeterminations = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->subject;
    }

    public function addSubjectDetermination(SubjectDetermination $subjectDetermination): self
    {
        if (!$this->subjectDeterminations->contains($subjectDetermination)) {
            $this->subjectDeterminations->add($subjectDetermination);
            $subjectDetermination->setSubject($this);
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function getSubjectDeterminations(): Collection
    {
        return $this->subjectDeterminations;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function setSubjectDeterminations(Collection $subjectDeterminations): self
    {
        $this->subjectDeterminations = $subjectDeterminations;

        return $this;
    }
}
