<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HylemObjectRepository")
 * @ORM\Cache(region="default", usage="NONSTRICT_READ_WRITE")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']])]
#[ApiFilter(OrderFilter::class, properties: ['id', 'hylemObject'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(SearchFilter::class, properties: ['hylemObject' => 'partial'])]
class HylemObject implements \Stringable
{
    /**
     * @ORM\Column(type="string", length=255)
     * @ApiProperty(iri="http://schema.org/name")
     * @Groups({"hylemObjectDetermination"})
     */
    private string $hylemObject = '';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HylemObjectDetermination", mappedBy="hylemObject", cascade={"persist"})
     */
    private Collection $hylemObjectDeterminations;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    public function __construct()
    {
        $this->hylemObjectDeterminations = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->hylemObject;
    }

    public function addHylemObjectDetermination(HylemObjectDetermination $hylemObjectDetermination): self
    {
        if (!$this->hylemObjectDeterminations->contains($hylemObjectDetermination)) {
            $this->hylemObjectDeterminations->add($hylemObjectDetermination);
            $hylemObjectDetermination->setHylemObject($this);
        }

        return $this;
    }

    public function getHylemObject(): ?string
    {
        return $this->hylemObject;
    }

    public function getHylemObjectDeterminations(): Collection
    {
        return $this->hylemObjectDeterminations;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setHylemObject(string $hylemObject): self
    {
        $this->hylemObject = $hylemObject;

        return $this;
    }

    public function setHylemObjectDeterminations(Collection $hylemObjectDeterminations): self
    {
        $this->hylemObjectDeterminations = $hylemObjectDeterminations;

        return $this;
    }
}
