<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubjectDeterminationRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']], normalizationContext: ['groups' => 'subjectDetermination'])]
class SubjectDetermination implements \Stringable
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hylem", inversedBy="subjectDeterminations", cascade={"persist"})
     * @Groups("subjectDetermination")
     */
    private ?Hylem $hylem = null;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subject", inversedBy="subjectDeterminations", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups("subjectDetermination")
     */
    private ?Subject $subject = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"subjectDetermination"})
     */
    private ?string $subjectDetermination = null;

    public function __toString(): string
    {
        return (string) $this->subjectDetermination;
    }

    public function getHylem(): ?Hylem
    {
        return $this->hylem;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?Subject
    {
        return $this->subject;
    }

    public function getSubjectDetermination(): ?string
    {
        return $this->subjectDetermination;
    }

    public function setHylem(?Hylem $hylem): self
    {
        $this->hylem = $hylem;

        return $this;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setSubject(?Subject $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function setSubjectDetermination(?string $subjectDetermination): self
    {
        $this->subjectDetermination = $subjectDetermination;

        return $this;
    }
}
