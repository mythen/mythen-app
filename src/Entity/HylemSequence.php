<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Stringable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HylemSequenceRepository")
 */
#[ApiResource]
#[ApiFilter(OrderFilter::class, properties: ['id', 'name'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(SearchFilter::class, properties: ['name' => 'partial', 'myths' => 'partial', 'sources' => 'partial'])]
class HylemSequence implements Stringable
{
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $editor = null;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @ApiProperty(iri="http://schema.org/identifier")
     */
    private ?int $id = null;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HylemSequenceItem", mappedBy="itemsTime", orphanRemoval=true, cascade={"persist"})
     */
    private Collection $itemsTime;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Myth", mappedBy="hylemSequences", cascade={"persist"})
     */
    private Collection $myths;

    /**
     * @ORM\Column(type="string")
     */
    private string $name = '';

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Source", mappedBy="hylemSequences", cascade={"persist"})
     */
    private Collection $sources;

    public function __construct()
    {
        $this->sources = new ArrayCollection();
        $this->myths = new ArrayCollection();
        $this->itemsTime = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->id.' '.$this->name;
    }

    public function addItemsTime(HylemSequenceItem $hylemSequenceItem): self
    {
        if (!$this->itemsTime->contains($hylemSequenceItem)) {
            $this->itemsTime->add($hylemSequenceItem);
            $hylemSequenceItem->setItemsTime($this);
        }

        return $this;
    }

    public function addMyth(Myth $myth): self
    {
        if (!$this->myths->contains($myth)) {
            $this->myths->add($myth);
            $myth->addHylemSequence($this);
        }

        return $this;
    }

    public function addSource(Source $source): self
    {
        if (!$this->sources->contains($source)) {
            $this->sources->add($source);
            $source->addHylemSequence($this);
        }

        return $this;
    }

    public function getEditor(): ?string
    {
        return $this->editor;
    }

    public function getHylemSequence(): ?int
    {
        return $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItemsTime(): Collection
    {
        return $this->itemsTime;
    }

    public function getMyths(): Collection
    {
        return $this->myths;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSources(): Collection
    {
        return $this->sources;
    }

    public function removeItemsTime(HylemSequenceItem $hylemSequenceItem): self
    {
        if ($this->itemsTime->contains($hylemSequenceItem)) {
            $this->itemsTime->removeElement($hylemSequenceItem);
            $hylemSequenceItem = null;
        }

        return $this;
    }

    public function removeMyth(Myth $myth): self
    {
        if ($this->myths->contains($myth)) {
            $this->myths->removeElement($myth);
            $myth->removeHylemSequence($this);
        }

        return $this;
    }

    public function removeSource(Source $source): self
    {
        if ($this->sources->contains($source)) {
            $this->sources->removeElement($source);
            $source->removeHylemSequence($this);
        }

        return $this;
    }

    public function setEditor(?string $editor): self
    {
        $this->editor = $editor;

        return $this;
    }

    public function setHylemSequence(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setItemsTime(Collection $itemsTime): self
    {
        $this->itemsTime = $itemsTime;

        return $this;
    }

    public function setMyths(Collection $myths): self
    {
        $this->myths = $myths;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setSources(Collection $sources): self
    {
        $this->sources = $sources;

        return $this;
    }
}
