<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Stringable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HeadlineRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']])]
class Headline implements Stringable
{
    /**
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/name")
     */
    private ?string $headlineRemarks = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HylemSequenceItem", mappedBy="headline", cascade={"persist"})
     */
    private Collection $hylemSequenceItems;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    public function __construct()
    {
        $this->hylemSequenceItems = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->headlineRemarks;
    }

    public function addHylemSequenceItem(HylemSequenceItem $hylemSequenceItem): self
    {
        if (!$this->hylemSequenceItems->contains($hylemSequenceItem)) {
            $this->hylemSequenceItems->add($hylemSequenceItem);
            $hylemSequenceItem->setHeadline($this);
        }

        return $this;
    }

    public function getHeadlineRemarks(): ?string
    {
        return $this->headlineRemarks;
    }

    public function getHylemSequenceItems(): Collection
    {
        return $this->hylemSequenceItems;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function removeHylemSequenceItem(HylemSequenceItem $hylemSequenceItem): self
    {
        if ($this->hylemSequenceItems->contains($hylemSequenceItem)) {
            $this->hylemSequenceItems->removeElement($hylemSequenceItem);
            // set the owning side to null (unless already changed)
            if ($hylemSequenceItem->getHeadline() === $this) {
                $hylemSequenceItem->setHeadline(null);
            }
        }

        return $this;
    }

    public function setHeadlineRemarks(?string $headlineRemarks): self
    {
        $this->headlineRemarks = $headlineRemarks;

        return $this;
    }

    public function setHylemSequenceItems(Collection $hylemSequenceItems): Headline
    {
        $this->hylemSequenceItems = $hylemSequenceItems;

        return $this;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }
}
