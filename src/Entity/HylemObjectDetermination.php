<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HylemObjectDeterminationRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']], normalizationContext: ['groups' => ['hylemObjectDetermination']])]
class HylemObjectDetermination implements \Stringable
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hylem", inversedBy="hylemObjectDeterminations", cascade={"persist"})
     * @Groups("hylemObjectDetermination")
     */
    private ?Hylem $hylem = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HylemObject", inversedBy="hylemObjectDeterminations", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups("hylemObjectDetermination")
     */
    private ?HylemObject $hylemObject = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("hylemObjectDetermination")
     */
    private ?string $hylemObjectDetermination = null;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    public function __toString(): string
    {
        return (string) $this->hylemObjectDetermination;
    }

    public function getHylem(): ?Hylem
    {
        return $this->hylem;
    }

    public function getHylemObject(): ?HylemObject
    {
        return $this->hylemObject;
    }

    public function getHylemObjectDetermination(): ?string
    {
        return $this->hylemObjectDetermination;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setHylem(?Hylem $hylem): self
    {
        $this->hylem = $hylem;

        return $this;
    }

    public function setHylemObject(?HylemObject $hylemObject): self
    {
        $this->hylemObject = $hylemObject;

        return $this;
    }

    public function setHylemObjectDetermination(?string $hylemObjectDetermination): self
    {
        $this->hylemObjectDetermination = $hylemObjectDetermination;

        return $this;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }
}
