<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SourceLocalityRepository")
 */
#[ApiResource(attributes: ['order' => ['id' => 'DESC']])]
#[ApiFilter(OrderFilter::class, properties: ['id', 'placeName'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(SearchFilter::class, properties: ['placeName' => 'partial'])]
class SourceLocality implements \Stringable
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $gettyID = null;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @ApiProperty(iri="http://schema.org/name")
     */
    private string $placeName = '';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Source", mappedBy="locality", cascade={"persist"})
     */
    private Collection $sources;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $wikidataID = null;

    public function __construct()
    {
        $this->sources = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->placeName;
    }

    public function addSource(Source $source): self
    {
        if (!$this->sources->contains($source)) {
            $this->sources[] = $source;
            $source->setLocality($this);
        }

        return $this;
    }

    public function getGettyID(): ?string
    {
        return $this->gettyID;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlaceName(): ?string
    {
        return $this->placeName;
    }

    public function getSources(): Collection
    {
        return $this->sources;
    }

    public function getWikidataID(): ?string
    {
        return $this->wikidataID;
    }

    public function removeSource(Source $source): self
    {
        if ($this->sources->contains($source)) {
            $this->sources->removeElement($source);
            // set the owning side to null (unless already changed)
            if ($source->getLocality() === $this) {
                $source->setLocality(null);
            }
        }

        return $this;
    }

    public function setGettyID(?string $gettyID): self
    {
        $this->gettyID = $gettyID;

        return $this;
    }

    public function setPlaceName(string $placeName): self
    {
        $this->placeName = $placeName;

        return $this;
    }

    public function setSources(Collection $sources): self
    {
        $this->sources = $sources;

        return $this;
    }

    public function setWikidataID(?string $wikidataID): self
    {
        $this->wikidataID = $wikidataID;

        return $this;
    }
}
