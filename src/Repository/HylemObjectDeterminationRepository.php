<?php

namespace App\Repository;

use App\Entity\HylemObjectDetermination;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HylemObjectDetermination|null find($id, $lockMode = null, $lockVersion = null)
 * @method HylemObjectDetermination|null findOneBy(array $criteria, array $orderBy = null)
 * @method HylemObjectDetermination[]    findAll()
 * @method HylemObjectDetermination[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HylemObjectDeterminationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HylemObjectDetermination::class);
    }
}
