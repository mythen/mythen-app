<?php

namespace App\Repository;

use App\Entity\HylemSequenceItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HylemSequenceItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method HylemSequenceItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method HylemSequenceItem[]    findAll()
 * @method HylemSequenceItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HylemSequenceItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HylemSequenceItem::class);
    }
}
