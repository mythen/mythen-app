<?php

namespace App\Repository;

use App\Entity\SourceLocality;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SourceLocality|null find($id, $lockMode = null, $lockVersion = null)
 * @method SourceLocality|null findOneBy(array $criteria, array $orderBy = null)
 * @method SourceLocality[]    findAll()
 * @method SourceLocality[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SourceLocalityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SourceLocality::class);
    }
}
