<?php

namespace App\Repository;

use App\Entity\Headline;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Headline|null find($id, $lockMode = null, $lockVersion = null)
 * @method Headline|null findOneBy(array $criteria, array $orderBy = null)
 * @method Headline[]    findAll()
 * @method Headline[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HeadlineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Headline::class);
    }
}
