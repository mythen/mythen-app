<?php

namespace App\Repository;

use App\Entity\SubjectDetermination;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SubjectDetermination|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubjectDetermination|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubjectDetermination[]    findAll()
 * @method SubjectDetermination[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubjectDeterminationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubjectDetermination::class);
    }
}
