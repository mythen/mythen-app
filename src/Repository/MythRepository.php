<?php

namespace App\Repository;

use App\Entity\Myth;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Myth|null find($id, $lockMode = null, $lockVersion = null)
 * @method Myth|null findOneBy(array $criteria, array $orderBy = null)
 * @method Myth[]    findAll()
 * @method Myth[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MythRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Myth::class);
    }
}
