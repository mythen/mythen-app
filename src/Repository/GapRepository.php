<?php

namespace App\Repository;

use App\Entity\Gap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Gap|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gap|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gap[]    findAll()
 * @method Gap[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GapRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gap::class);
    }
}
