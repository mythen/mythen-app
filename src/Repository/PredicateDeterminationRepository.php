<?php

namespace App\Repository;

use App\Entity\PredicateDetermination;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PredicateDetermination|null find($id, $lockMode = null, $lockVersion = null)
 * @method PredicateDetermination|null findOneBy(array $criteria, array $orderBy = null)
 * @method PredicateDetermination[]    findAll()
 * @method PredicateDetermination[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PredicateDeterminationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PredicateDetermination::class);
    }
}
