<?php

namespace App\Repository;

use App\Entity\SourceObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SourceObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method SourceObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method SourceObject[]    findAll()
 * @method SourceObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SourceObjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SourceObject::class);
    }
}
