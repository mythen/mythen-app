<?php

namespace App\Repository;

use App\Entity\HylemObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HylemObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method HylemObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method HylemObject[]    findAll()
 * @method HylemObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HylemObjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HylemObject::class);
    }
}
