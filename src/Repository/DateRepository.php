<?php

namespace App\Repository;

use App\Entity\CustomDate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CustomDate|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomDate|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomDate[]    findAll()
 * @method CustomDate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomDate::class);
    }
}
