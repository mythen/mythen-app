<?php

namespace App\Repository;

use App\Entity\HylemSequence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HylemSequence|null find($id, $lockMode = null, $lockVersion = null)
 * @method HylemSequence|null findOneBy(array $criteria, array $orderBy = null)
 * @method HylemSequence[]    findAll()
 * @method HylemSequence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HylemSequenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HylemSequence::class);
    }
}
