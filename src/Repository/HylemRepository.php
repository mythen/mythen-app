<?php

namespace App\Repository;

use App\Entity\Hylem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Hylem|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hylem|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hylem[]    findAll()
 * @method Hylem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HylemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hylem::class);
    }
}
