<?php

declare(strict_types=1);

namespace App\Import;

use Solarium\Client;
use Solarium\Core\Client\Adapter\Curl;
use Solarium\QueryType\Select\Query\Query;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SolrImport
{
    private Client $client;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $config = [
            'endpoint' => [
                'localhost' => [
                    'host' => 'solr',
                    'port' => 8_983,
                    'path' => '/',
                    'core' => 'myth',
                ],
            ],
        ];

        // @see https://github.com/solariumphp/solarium/issues/779#issuecomment-614105137
        $adapter = new Curl();

        $this->client = new Client($adapter, $eventDispatcher, $config);
    }

    public function getResults(): array
    {
        /** @var Query $query */
        $query = $this->client->createSelect();
        $query->setRows(8000);

        // this executes the query and returns the result
        $resultset = $this->client->execute($query);

        return $resultset->getData()['response']['docs'];
    }
}
