<?php

declare(strict_types=1);

namespace App\Import;

use App\Entity\Hylem;
use App\Entity\HylemObject;
use App\Entity\HylemObjectDetermination;
use App\Entity\HylemSequence;
use App\Entity\Predicate;
use App\Entity\PredicateDetermination;
use App\Entity\Subject;
use App\Entity\SubjectDetermination;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ImportUtility
{
    private static array $hylemNames = [];

    public function __construct(private EntityManagerInterface $manager, private LoggerInterface $logger)
    {
    }

    /**
     * @psalm-suppress UndefinedClass
     */
    public function getHylemSequence(array $hylemData): HylemSequence
    {
        $source = $hylemData['source'];

        if ($source && is_array($source)) {
            $source = implode(', ', $source);
        }

        if (array_key_exists($source, self::$hylemNames)) {
            return self::$hylemNames[$source];
        }

        $hylemSequence = $this->manager
            ->getRepository(HylemSequence::class)
            ->findOneBy([
                'name' => $source,
            ]);

        if (null === $hylemSequence) {
            $this->logger->info(sprintf('Add new HylemSequence %s', $source));
            $hylemSequence = new HylemSequence();
            $hylemSequence->setName($source);
            self::$hylemNames[$source] = $hylemSequence;
        }

        return $hylemSequence;
    }

    /**
     * @psalm-suppress InvalidArgument
     */
    public function getObjectDeterminations(array $hylemData, Hylem $hylem): ArrayCollection
    {
        $objectDeterminations = new ArrayCollection();

        if (array_key_exists('object', $hylemData)) {
            foreach ($hylemData['object'] as $objectData) {
                $object = $this->checkForExistingElement($objectData, HylemObject::class, 'hylemObject');

                if (array_key_exists('object_determ', $hylemData)) {
                    foreach ($hylemData['object_determ'] as $objDetKey => $objDetValue) {
                        if ($hylemData['object_determ_ref'][$objDetKey] === $objectData) {
                            $objectDetermination = new HylemObjectDetermination();
                            $objectDetermination
                                ->setHylemObject($object)
                                ->setHylem($hylem)
                                ->setHylemObjectDetermination($objDetValue);

                            $objectDeterminations->add($objectDetermination);
                        }
                    }
                } else {
                    $objectDetermination = new HylemObjectDetermination();
                    $objectDetermination
                        ->setHylemObject($object)
                        ->setHylem($hylem)
                        ->setHylemObjectDetermination(null);

                    $objectDeterminations->add($objectDetermination);
                }
            }
        }

        return $objectDeterminations;
    }

    public function getPredicateDetermination(array $hylemData, Hylem $hylem): PredicateDetermination
    {
        $predicateData = $hylemData['predicate'];

        if ($predicateData && is_array($predicateData)) {
            $predicateData = implode(',', $predicateData);
        }
        $predicate = $this->checkForExistingElement($predicateData, Predicate::class, 'predicate');

        if (array_key_exists('predicate_determ', $hylemData)) {
            $predicateDeterminationText = is_array($hylemData['predicate_determ']) ? implode(',',
                $hylemData['predicate_determ']) : $hylemData['predicate_determ'];
        }
        $predicateDetermination = new PredicateDetermination();
        $predicateDetermination
            ->setPredicate($predicate)
            ->setHylem($hylem)
            ->setPredicateDetermination($predicateDeterminationText ?? null);

        return $predicateDetermination;
    }

    /**
     * @psalm-suppress InvalidArgument
     */
    public function getSubjectDeterminations(array $hylemData, Hylem $hylem): ArrayCollection
    {
        $subjectDeterminations = new ArrayCollection();
        $hylemDataSubject = $hylemData['subject'] ?? [];

        foreach ($hylemDataSubject as $subjectData) {
            $subject = $this->checkForExistingElement($subjectData, Subject::class, 'subject');

            if (array_key_exists('subject_determ', $hylemData)) {
                foreach ($hylemData['subject_determ'] as $subjDetKey => $subjDetValue) {
                    if ($hylemData['subject_determ_ref'][$subjDetKey] === $subjectData) {
                        $subjectDetermination = new SubjectDetermination();
                        $subjectDetermination
                            ->setSubject($subject)
                            ->setHylem($hylem)
                            ->setSubjectDetermination($subjDetValue);

                        $subjectDeterminations->add($subjectDetermination);
                    }
                }
            } else {
                $subjectDetermination = new SubjectDetermination();
                $subjectDetermination
                    ->setSubject($subject)
                    ->setHylem($hylem)
                    ->setSubjectDetermination(null);
                $subjectDeterminations->add($subjectDetermination);
            }
        }

        return $subjectDeterminations;
    }

    private function checkForExistingElement($element, string $class, string $field)
    {
        $existingElement = $this->manager->getRepository($class)->findOneBy([$field => $element]);

        if (null === $existingElement) {
            $this->logger->info(sprintf('Add new %s for %s: %s', $field, $class, $element));

            $methodName = 'set'.ucfirst($field);
            $existingElement = (new $class())->$methodName($element);
        }

        return $existingElement;
    }
}
