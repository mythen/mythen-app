<?php

declare(strict_types=1);

namespace App\DataEnricher;

use App\Entity\Hylem;
use App\Entity\HylemObjectDetermination;
use App\Entity\SubjectDetermination;

class HySpoGenerator
{
    public function generateHySpo(Hylem $hylem): string
    {
        /** @var SubjectDetermination $firstSubjectDetermination */
        $firstSubjectDetermination = $hylem->getSubjectDeterminations()->first() ?: new SubjectDetermination();
        $subject = $firstSubjectDetermination->getSubject();

        $hySpoSubject = (null !== $subject ? (string) $subject->getSubject() : '');

        $predicateDetermination = $hylem->getPredicateDetermination();

        if (null !== $predicateDetermination) {
            $predicate = $predicateDetermination->getPredicate();
            $hyspoPredicate = (null !== $predicate ? (string) $predicate->getPredicate() : '');
        } else {
            $hyspoPredicate = '';
        }

        $firstHylemObjectDetermination = $hylem->getHylemObjectDeterminations()->first();
        $hylemObject = ($firstHylemObjectDetermination instanceof HylemObjectDetermination) ? $firstHylemObjectDetermination->getHylemObject() : null;

        $hyspoHylemObject = (null !== $hylemObject ? (string) $hylemObject->getHylemObject() : '');

        return sprintf('%s %s %s', $hySpoSubject, $hyspoPredicate, $hyspoHylemObject);
    }
}
