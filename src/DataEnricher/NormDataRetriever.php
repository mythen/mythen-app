<?php

declare(strict_types=1);

namespace App\DataEnricher;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;

final class NormDataRetriever
{
    /**
     * @var string
     */
    private const GETTY_ID = 'P1667';

    /**
     * @var string
     */
    private const GND_ID = 'P227';

    public function __construct(private LoggerInterface $logger)
    {
    }

    public function getGettyIdFromWikidata(string $wikidataId): ?string
    {
        return $this->getWikidata($wikidataId, self::GETTY_ID);
    }

    public function getGndIdFromWikidata(string $wikidataId): ?string
    {
        return $this->getWikidata($wikidataId, self::GND_ID);
    }

    private function getWikidata(string $wikidataId, string $field): ?string
    {
        $httpClient = HttpClient::create();
        $data = $httpClient->request('GET', sprintf('https://www.wikidata.org/wiki/Special:EntityData/%s.json', $wikidataId))->getContent();
        $dataset = json_decode($data, false, 512, JSON_THROW_ON_ERROR)->entities->$wikidataId->claims;

        try {
            return $dataset->$field[0]->mainsnak->datavalue->value;
        } catch (\Throwable $throwable) {
            $this->logger->error($throwable->getMessage(), $throwable->getTrace());

            return null;
        }
    }
}
