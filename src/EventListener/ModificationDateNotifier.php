<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Source;
use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;

class ModificationDateNotifier
{
    public function prePersist(Source $source, LifecycleEventArgs $event): void
    {
        if (null === $source->getCreated()) {
            $source->setCreated(new DateTime());
        }
        $source->setModified(new DateTime());
    }
}
