<?php

declare(strict_types=1);

namespace App\EventListener;

use App\DataEnricher\HySpoGenerator;
use App\Entity\Hylem;
use Doctrine\ORM\Event\LifecycleEventArgs;

class HySpoNotifier
{
    public function __construct(private HySpoGenerator $hyspoGenerator)
    {
    }

    public function prePersist(Hylem $hylem, LifecycleEventArgs $event): void
    {
        $hySpo = $hylem->getHySpo();

        if (null === $hySpo || '' === $hySpo) {
            $hylem->setHySpo($this->hyspoGenerator->generateHySpo($hylem));
        }
    }
}
