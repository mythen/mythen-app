<?php

declare(strict_types=1);

namespace App\EventListener;

use App\DataEnricher\NormDataRetriever;
use App\Entity\SourceLocality;
use Doctrine\ORM\Event\LifecycleEventArgs;

class SourceLocalityNotifier
{
    public function __construct(private NormDataRetriever $dataRetriever)
    {
    }

    public function prePersist(SourceLocality $locality, LifecycleEventArgs $event): void
    {
        $wikidataId = $locality->getWikidataID();

        if (null !== $wikidataId && null === $locality->getGettyID()) {
            $locality->setGettyID($this->dataRetriever->getGettyIdFromWikidata($wikidataId));
        }
    }
}
