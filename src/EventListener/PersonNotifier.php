<?php

declare(strict_types=1);

namespace App\EventListener;

use App\DataEnricher\NormDataRetriever;
use App\Entity\Person;
use Doctrine\ORM\Event\LifecycleEventArgs;

class PersonNotifier
{
    public function __construct(private NormDataRetriever $dataRetriever)
    {
    }

    public function prePersist(Person $person, LifecycleEventArgs $event): void
    {
        $wikidataId = $person->getWikidataID();

        if (null !== $wikidataId && (null === $person->getGndID() || '' === $person->getGndID())) {
            $person->setGndID($this->dataRetriever->getGndIdFromWikidata($wikidataId));
        }
    }
}
