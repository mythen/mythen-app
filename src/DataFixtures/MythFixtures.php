<?php

namespace App\DataFixtures;

use App\Entity\Myth;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class MythFixtures extends BaseFixture implements DependentFixtureInterface
{
    /**
     * @var string
     */
    public const MYTH_URL = 'https://staging.myth.sub.uni-goettingen.de/api/myths';

    public function getDependencies()
    {
        return [
            SourceFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        $content = $this->getResponse(self::MYTH_URL, Myth::class, ['sources']);

        /** @var Myth $myth */
        foreach ($content as $myth) {
            $manager->persist($myth);
        }

        $manager->flush();
    }
}
