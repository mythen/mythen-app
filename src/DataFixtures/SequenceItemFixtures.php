<?php

namespace App\DataFixtures;

use App\Entity\Gap;
use App\Entity\Headline;
use App\Entity\Hylem;
use App\Entity\HylemObject;
use App\Entity\HylemObjectDetermination;
use App\Entity\HylemSequence;
use App\Entity\HylemSequenceItem;
use App\Entity\Predicate;
use App\Entity\PredicateDetermination;
use App\Entity\Subject;
use App\Entity\SubjectDetermination;
use App\Import\SolrImport;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;

final class SequenceItemFixtures extends Fixture implements DependentFixtureInterface
{
    private static array $hylemNames = [];
    private array $references;

    public function __construct(private SolrImport $solrImport)
    {
    }

    public function getDependencies(): array
    {
        return [
            SourceFixtures::class,
            SubjectFixtures::class,
            PredicateFixtures::class,
            HylemObjectFixtures::class,
            TypeFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        ini_set('memory_limit', '-1');
        $this->references = $this->referenceRepository->getReferences();

        $data = $this->solrImport->getResults();

        foreach ($data as $hylemData) {
            $hylem = new Hylem();

            $lineInfo = $hylemData['line_info'];
            if ($lineInfo && is_array($lineInfo)) {
                $lineInfo = implode(', ', $hylemData['line_info']);
            }

            $hspo = $hylemData['hspo'];
            if ($hspo && is_array($hspo)) {
                $hspo = implode(', ', $hylemData['hspo']);
            }

            $hylem
                ->setTextReference($lineInfo)
                ->setHySpo($hspo)
                ->setImplicit(false)
                // There is no Solr field 'indirect'. Therefore we setIndirect to the inverted value of 'speach'.
                ->setIndirect(is_bool($hylemData['speach']) ? !$hylemData['speach'] : true)
                ->setSubjectDeterminations($this->getSubjectDeterminations($hylemData, $hylem))
                ->setPredicateDetermination($this->getPredicateDetermination($hylemData, $hylem))
                ->setHylemObjectDeterminations($this->getObjectDeterminations($hylemData, $hylem))
                ->setType($this->getReference(sprintf('type-0')));

            $hylemSequenceItem = new HylemSequenceItem();

            $hylemSequence = $this->getHylemSequence($manager, $hylemData);

            $hylem->addHylemSequenceItem($hylemSequenceItem);
            $hylemSequenceItem
                ->setHylem($hylem)
                ->setItemType('Hylem')
                ->setStorySort($hylemSequence->getItemsTime()->count())
                ->setChronSort($hylemSequence->getItemsTime()->count());

            $hylemSequence->addItemsTime($hylemSequenceItem);

            try {
                if (1 !== random_int(0, 1)) {
                    $gapOrHeadling = random_int(0, 1);
                    if (0 == $gapOrHeadling) {
                        $gap = new Gap();
                        $hylemSequenceItem = new HylemSequenceItem();

                        $gap
                            ->addHylemSequenceItem($hylemSequenceItem)
                            ->setGapRemarks('Test Gap: Sequence-Item No. '.($hylemSequence->getItemsTime()->count()));
                        $hylemSequenceItem
                            ->setGap($gap)
                            ->setItemType('Gap')
                            ->setStorySort($hylemSequence->getItemsTime()->count())
                            ->setChronSort($hylemSequence->getItemsTime()->count());
                        $hylemSequence->addItemsTime($hylemSequenceItem);
                    }

                    if (1 == 1) {
                        $headline = new Headline();
                        $hylemSequenceItem = new HylemSequenceItem();

                        $headline
                            ->addHylemSequenceItem($hylemSequenceItem)
                            ->setHeadlineRemarks('Test Headline: Sequence-Item No. '.($hylemSequence->getItemsTime()->count()));
                        $hylemSequenceItem
                            ->setHeadline($headline)
                            ->setItemType('Headline')
                            ->setStorySort($hylemSequence->getItemsTime()->count())
                            ->setChronSort($hylemSequence->getItemsTime()->count());
                        $hylemSequence->addItemsTime($hylemSequenceItem);
                    }
                }
            } catch (Exception) {
            }

            $manager->persist($hylemSequence);
        }
        $manager->flush();
    }

    private function checkForExistingHylemObject(string $hylemObject): ?string
    {
        /** @var HylemObject $reference */
        foreach ($this->references as $key => $reference) {
            if (($reference instanceof HylemObject) && (trim($reference->getHylemObject()) === trim($hylemObject))) {
                return $key;
            }
        }

        return null;
    }

    private function checkForExistingPredicate(string $predicate): ?string
    {
        /** @var Predicate $reference */
        foreach ($this->references as $key => $reference) {
            if (($reference instanceof Predicate) && (trim($reference->getPredicate()) === trim($predicate))) {
                return $key;
            }
        }

        return null;
    }

    private function checkForExistingSubject(string $subject): ?string
    {
        /** @var Subject $reference */
        foreach ($this->references as $key => $reference) {
            if (($reference instanceof Subject) && (trim($reference->getSubject()) === trim($subject))) {
                return $key;
            }
        }

        return null;
    }

    private function getHylemSequence(ObjectManager $manager, array $hylemData): HylemSequence
    {
        $source = $hylemData['source'];

        if ($source && is_array($source)) {
            $source = implode(', ', $source);
        }

        if (array_key_exists($source, self::$hylemNames)) {
            return self::$hylemNames[$source];
        }

        $hylemSequence = $manager
            ->getRepository(HylemSequence::class)
            ->findOneBy([
                'name' => $source,
            ]);

        if (null === $hylemSequence) {
            $hylemSequence = new HylemSequence();
            $hylemSequence->setName($source);
            self::$hylemNames[$source] = $hylemSequence;
        }

        return $hylemSequence;
    }

    private function getObjectDeterminations(array $hylemData, Hylem $hylem): ArrayCollection
    {
        $objectDeterminations = new ArrayCollection();

        if (array_key_exists('object', $hylemData)) {
            foreach ($hylemData['object'] as $objectData) {
                $existingHylemObject = $this->checkForExistingHylemObject($objectData);
                if (null !== $existingHylemObject) {
                    $object = $this->getReference($existingHylemObject);
                } else {
                    $object = (new HylemObject())->setHylemObject($objectData);
                }

                if (array_key_exists('object_determ', $hylemData)) {
                    foreach ($hylemData['object_determ'] as $objDetKey => $objDetValue) {
                        if ($hylemData['object_determ_ref'][$objDetKey] === $objectData) {
                            $objectDetermination = new HylemObjectDetermination();
                            $objectDetermination
                                ->setHylemObject($object)
                                ->setHylem($hylem)
                                ->setHylemObjectDetermination($objDetValue);

                            $objectDeterminations->add($objectDetermination);
                        }
                    }
                } else {
                    $objectDetermination = new HylemObjectDetermination();
                    $objectDetermination
                        ->setHylemObject($object)
                        ->setHylem($hylem)
                        ->setHylemObjectDetermination(null);

                    $objectDeterminations->add($objectDetermination);
                }
            }
        }

        return $objectDeterminations;
    }

    private function getPredicateDetermination(array $hylemData, Hylem $hylem): PredicateDetermination
    {
        $predicateData = $hylemData['predicate'];

        if ($predicateData && is_array($predicateData)) {
            $predicateData = implode(',', $predicateData);
        }
        $existingPredicate = $this->checkForExistingPredicate($predicateData);

        if (null !== $existingPredicate) {
            $predicate = $this->getReference($existingPredicate);
        } else {
            $predicate = (new Predicate())->setPredicate($predicateData);
        }

        if (array_key_exists('predicate_determ', $hylemData)) {
            $predicateDeterminationText = is_array($hylemData['predicate_determ']) ? implode(',',
                $hylemData['predicate_determ']) : $hylemData['predicate_determ'];
        }
        $predicateDetermination = new PredicateDetermination();
        $predicateDetermination
            ->setPredicate($predicate)
            ->setHylem($hylem)
            ->setPredicateDetermination($predicateDeterminationText ?? null);

        return $predicateDetermination;
    }

    private function getSubjectDeterminations(array $hylemData, Hylem $hylem): ArrayCollection
    {
        $subjectDeterminations = new ArrayCollection();
        $hylemDataSubject = $hylemData['subject'] ?? [];

        foreach ($hylemDataSubject as $subjectData) {
            $existingSubject = $this->checkForExistingSubject($subjectData);
            if (null !== $existingSubject) {
                $subject = $this->getReference($existingSubject);
            } else {
                $subject = (new Subject())->setSubject($subjectData);
            }

            if (array_key_exists('subject_determ', $hylemData)) {
                foreach ($hylemData['subject_determ'] as $subjDetKey => $subjDetValue) {
                    if ($hylemData['subject_determ_ref'][$subjDetKey] === $subjectData) {
                        $subjectDetermination = new SubjectDetermination();
                        $subjectDetermination
                            ->setSubject($subject)
                            ->setHylem($hylem)
                            ->setSubjectDetermination($subjDetValue);

                        $subjectDeterminations->add($subjectDetermination);
                    }
                }
            } else {
                $subjectDetermination = new SubjectDetermination();
                $subjectDetermination
                    ->setSubject($subject)
                    ->setHylem($hylem)
                    ->setSubjectDetermination(null);

                $subjectDeterminations->add($subjectDetermination);
            }
        }

        return $subjectDeterminations;
    }
}
