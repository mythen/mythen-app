<?php

namespace App\DataFixtures;

use App\Entity\SourceObject;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

final class SourceObjectFixtures extends BaseFixture implements FixtureGroupInterface
{
    /**
     * @var string
     */
    public const URL = 'https://staging.myth.sub.uni-goettingen.de/api/source_objects';

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to.
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['standardizedGroup'];
    }

    public function load(ObjectManager $manager): void
    {
        $content = $this->getResponse(self::URL, SourceObject::class, ['sources']);

        /** @var SourceObject $sourceObject */
        foreach ($content as $sourceObject) {
            $manager->persist($sourceObject);
        }

        $manager->flush();
    }
}
