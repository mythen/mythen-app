<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class UserFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private UserPasswordEncoderInterface $passwordEncoder)
    {
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on.
     */
    public function getDependencies(): array
    {
        return [
            UserTeamFixtures::class,
        ];
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to.
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['userGroup'];
    }

    public function load(ObjectManager $manager): void
    {
        $userAdmin = new User();
        $userReadOnly = new User();

        $userAdmin
            ->setUsername('admin')
            ->setFullname('Myth Admin')
            ->setEmail('mythen.admin@sub.uni-goettingen.de')
            ->addTeam($this->getReference(sprintf('user-team-1')))
            ->setRoles(['ROLE_ADMIN'])
            ->setPassword($this->passwordEncoder->encodePassword(
                $userAdmin,
            'admin'
        ));

        $userReadOnly
            ->setUsername('user')
            ->setFullname('Myth User')
            ->setEmail('mythen.user@sub.uni-goettingen.de')
            ->addTeam($this->getReference(sprintf('user-team-2')))
            ->setRoles(['ROLE_USER'])
            ->setPassword($this->passwordEncoder->encodePassword(
                $userReadOnly,
            'user'
        ));

        $manager->persist($userAdmin);
        $manager->flush();

        $manager->persist($userReadOnly);
        $manager->flush();
    }
}
