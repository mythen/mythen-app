<?php

namespace App\DataFixtures;

use App\Entity\Predicate;
use App\Import\SolrImport;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class PredicateFixtures extends Fixture
{
    private array $references;

    public function __construct(private SolrImport $solrImport)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $data = $this->solrImport->getResults();

        $i = 0;
        foreach ($data as $hylemData) {
            if (array_key_exists('predicate', $hylemData)) {
                $predicateData = $hylemData['predicate'];
                $this->references = $this->referenceRepository->getReferences();
                if (!is_string($predicateData)) {
                    $predicateData = implode(', ', $predicateData);
                }
                $existingPredicate = $this->checkForExistingPredicate($predicateData);

                if (null === $existingPredicate) {
                    $predicate = (new Predicate())->setPredicate($predicateData);
                    $manager->persist($predicate);
                    $this->addReference(sprintf('predicate-%d', $i), $predicate);
                    ++$i;
                }
            }
        }

        $manager->flush();
    }

    private function checkForExistingPredicate(string $predicate): ?string
    {
        /** @var Predicate $reference */
        foreach ($this->references as $key => $reference) {
            if (($reference instanceof Predicate) && (trim($reference->getPredicate()) === trim($predicate))) {
                return $key;
            }
        }

        return null;
    }
}
