<?php

namespace App\DataFixtures;

use App\Entity\Type;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class TypeFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $types = ['dynamisch-punktuell', 'statisch-durativ', 'statisch-resultativ', 'dynamisch-durativ'];

        /* @var Type $type */
        foreach ($types as $typeKey => $typeTitle) {
            $type = new Type();
            $type->setType($typeTitle);

            $manager->persist($type);
            $this->setReference(sprintf('type-%s', $typeKey), $type);

            $manager->flush();
        }
    }
}
