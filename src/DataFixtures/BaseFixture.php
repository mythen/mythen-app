<?php

declare(strict_types=1);

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ObjectManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class BaseFixture extends Fixture
{
    public function __construct(private SerializerInterface $serializer, private LoggerInterface $logger)
    {
    }

    public function load(ObjectManager $manager): void
    {
        // TODO: Implement load() method.
    }

    protected function getResponse(string $url, string $type, array $ignoredFields = []): ArrayCollection
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $url,
            [
                'headers' => [
                    'accept' => 'application/json',
                ],
            ]
        );

        try {
            $content = $this->serializer->deserialize(
                $response->getContent(),
                $type.'[]',
                'json',
                [
                    AbstractNormalizer::IGNORED_ATTRIBUTES => $ignoredFields,
                ]
            );
        } catch (ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface | TransportExceptionInterface $e) {
            $content = new ArrayCollection();
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        return new ArrayCollection($content);
    }
}
