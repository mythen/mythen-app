<?php

namespace App\DataFixtures;

use App\Entity\HylemObject;
use App\Import\SolrImport;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

final class HylemObjectFixtures extends Fixture
{
    private array $references;

    public function __construct(private SolrImport $solrImport, private EntityManagerInterface $manager)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $data = $this->solrImport->getResults();

        $i = 0;
        foreach ($data as $hylemData) {
            if (array_key_exists('object', $hylemData)) {
                foreach ($hylemData['object'] as $hylemObjectData) {
                    $this->references = $this->referenceRepository->getReferences();
                    $existingHylemObject = $this->checkForExistingHylemObject($hylemObjectData);

                    if (null === $existingHylemObject) {
                        $hylemObject = (new HylemObject())->setHylemObject($hylemObjectData);
                        $manager->persist($hylemObject);
                        $this->addReference(sprintf('object-%d', $i), $hylemObject);
                        ++$i;
                    }
                }
            }
        }

        $manager->flush();
    }

    private function checkForExistingHylemObject(string $hylemObject): ?string
    {
        /** @var HylemObject $reference */
        foreach ($this->references as $key => $reference) {
            if (($reference instanceof HylemObject) && (trim($reference->getHylemObject()) === trim($hylemObject))) {
                return $key;
            }
        }

        return null;
    }
}
