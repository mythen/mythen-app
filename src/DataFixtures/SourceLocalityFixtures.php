<?php

namespace App\DataFixtures;

use App\Entity\SourceLocality;
use Doctrine\Persistence\ObjectManager;

final class SourceLocalityFixtures extends BaseFixture
{
    /**
     * @var string
     */
    public const URL = 'https://staging.myth.sub.uni-goettingen.de/api/source_localities';

    public function load(ObjectManager $manager): void
    {
        $content = $this->getResponse(self::URL, SourceLocality::class, ['sources']);

        /** @var SourceLocality $locality */
        foreach ($content as $locality) {
            $manager->persist($locality);
        }

        $manager->flush();
    }
}
