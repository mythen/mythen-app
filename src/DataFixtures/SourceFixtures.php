<?php

namespace App\DataFixtures;

use App\Entity\Source;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class SourceFixtures extends BaseFixture implements DependentFixtureInterface
{
    /**
     * @var string
     */
    public const URL = 'https://staging.myth.sub.uni-goettingen.de/api/sources';

    public function getDependencies(): array
    {
        return [
            SourceLocalityFixtures::class,
            SourceObjectFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        $content = $this->getResponse(self::URL, Source::class);

        /** @var Source $source */
        foreach ($content as $source) {
            $manager->persist($source);
        }
        $manager->flush();
    }
}
