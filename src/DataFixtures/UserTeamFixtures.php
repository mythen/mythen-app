<?php

namespace App\DataFixtures;

use App\Entity\UserTeam;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

final class UserTeamFixtures extends Fixture implements FixtureGroupInterface
{
    /**
     * This method must return an array of groups
     * on which the implementing class belongs to.
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['userGroup'];
    }

    public function load(ObjectManager $manager): void
    {
        $userTeamAdmin = new UserTeam();
        $userTeamReadOnly = new UserTeam();

        $userTeamAdmin
            ->setTeamName('Administratoren')
            ->setTeamDescription('Nutzer dieser Gruppe dürfen alle Datensätze bearbeiten und löschen.')
        ;

        $userTeamReadOnly
            ->setTeamName('Leser')
            ->setTeamDescription('Nutzer dieser Gruppe dürfen freigegebene Datensätze lesen.')
        ;

        $manager->persist($userTeamAdmin);
        $this->setReference(sprintf('user-team-%s', 1), $userTeamAdmin);
        $manager->flush();

        $manager->persist($userTeamReadOnly);
        $this->setReference(sprintf('user-team-%s', 2), $userTeamReadOnly);
        $manager->flush();
    }
}
