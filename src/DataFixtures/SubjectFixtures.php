<?php

namespace App\DataFixtures;

use App\Entity\Subject;
use App\Import\SolrImport;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

final class SubjectFixtures extends Fixture
{
    private array $references;

    public function __construct(private SolrImport $solrImport, private EntityManagerInterface $manager)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $data = $this->solrImport->getResults();

        $i = 0;
        foreach ($data as $hylemData) {
            if (array_key_exists('subject', $hylemData)) {
                foreach ($hylemData['subject'] as $subjectData) {
                    $this->references = $this->referenceRepository->getReferences();
                    $existingSubject = $this->checkForExistingSubject($subjectData);

                    if (null === $existingSubject) {
                        $subject = (new Subject())->setSubject($subjectData);
                        $manager->persist($subject);
                        $this->addReference(sprintf('subject-%d', $i), $subject);
                        ++$i;
                    }
                }
            }
        }

        $manager->flush();
    }

    private function checkForExistingSubject(string $subject): ?string
    {
        /** @var Subject $reference */
        foreach ($this->references as $key => $reference) {
            if (($reference instanceof Subject) && (trim($reference->getSubject()) === trim($subject))) {
                return $key;
            }
        }

        return null;
    }
}
