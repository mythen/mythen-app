<?php

namespace App\Form;

use App\Entity\HylemObject;
use App\Entity\HylemObjectDetermination;
use Doctrine\ORM\Cache;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HylemObjectDeterminationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('hylemObject', EntityType::class, [
                'class' => HylemObject::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')

                        ->setCacheable(true)
                        ->setCacheMode(Cache::MODE_NORMAL)
                        ->setCacheRegion('default')

                        ->orderBy('o.hylemObject', 'ASC');
                },
                'attr' => ['class' => 'select2-reload', 'data-widget' => 'select2'],
            ])
            ->add('hylemObjectDetermination', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => HylemObjectDetermination::class,
        ]);
    }
}
