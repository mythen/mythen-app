<?php

namespace App\Form;

use App\Entity\Hylem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HylemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id')
            ->add('subjectDeterminations', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => SubjectDeterminationType::class,
            ])
            ->add('predicateDetermination', PredicateDeterminationType::class)
            ->add('hylemObjectDeterminations', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => HylemObjectDeterminationType::class,
            ])
            ->add('hySpo')
            ->add('originalText', null, [
                'label' => 'originalText',
            ])
            ->add('textReference', null, [
                'label' => 'Reference text',
            ])
            ->add('translation', null, [
                'label' => 'Translation',
            ])
            ->add('implicit', null, [
                'label' => 'Implicit',
            ])
            ->add('indirect', null, [
                'label' => 'Indirect',
            ])
            ->add('type', null, [
                'attr' => [
                    'class' => 'select2-reload',
                    'data-widget' => 'select2',
                ],
                'label' => 'Type',
            ])
            ->add('notes', null, [
                'label' => 'Notes',
            ])
            ->add('storySort', HiddenType::class, [
                'attr' => [
                    'class' => 'storytime-position',
                ],
            ])
            ->add('chronSort', HiddenType::class, [
                'attr' => [
                    'class' => 'chronological-position',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Hylem::class,
        ]);
    }
}
