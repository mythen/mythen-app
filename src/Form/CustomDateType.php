<?php

namespace App\Form;

use App\Entity\CustomDate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomDateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('textual', TextType::class)
            ->add('start', DateType::class, [
                'widget' => 'single_text',
                'placeholder' => '1972',
                'input_format' => 'yyyy',
                'format' => 'yyyy',
                'html5' => false,
            ])
            ->add('end', DateType::class, [
                'widget' => 'single_text',
                'placeholder' => '1972',
                'input_format' => 'yyyy',
                'format' => 'yyyy',
                'html5' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CustomDate::class,
        ]);
    }
}
