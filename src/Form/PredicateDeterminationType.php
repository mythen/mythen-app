<?php

namespace App\Form;

use App\Entity\Predicate;
use App\Entity\PredicateDetermination;
use Doctrine\ORM\Cache;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PredicateDeterminationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('predicate', EntityType::class, [
                'class' => Predicate::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')

                        ->setCacheable(true)
                        ->setCacheMode(Cache::MODE_NORMAL)
                        ->setCacheRegion('default')

                        ->orderBy('p.predicate', 'ASC');
                },
                'attr' => ['class' => 'select2-reload', 'data-widget' => 'select2'],
            ])
            ->add('predicateDetermination', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PredicateDetermination::class,
        ]);
    }
}
