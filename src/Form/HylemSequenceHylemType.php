<?php

namespace App\Form;

use App\Entity\HylemSequenceItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HylemSequenceHylemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('hylem', HylemType::class)
            ->add('id', NumberType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => HylemSequenceItem::class,
            'empty_data' => new HylemSequenceItem(),
        ]);
    }

    public function getBlockPrefix()
    {
        return 'HylemSequenceItemType';
    }
}
