<?php

declare(strict_types=1);

namespace App\Controller;

use App\ValueObject\Routing\RouteName;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

class MainController extends AbstractController
{
    public function __construct(private RouterInterface $router)
    {
    }

    #[Route(path: '/', name: RouteName::HOME)]
    public function index(): RedirectResponse
    {
        // Redirect to /api until there is a presentation environment
        return new RedirectResponse($this->router->generate('api_doc', [], RouterInterface::ABSOLUTE_URL));
    }
}
